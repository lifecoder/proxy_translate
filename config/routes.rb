Rails.application.routes.draw do

  get '/api/v0/domains/map(.json)(/:extra)', to: 'api#map'
  get '/api/v0/process/:provider', to: 'api#xprocess'
  post '/api/v0/process/:provider', to: 'api#xprocess'
  #
  # get 'api_controller/map'
  #
  # get 'api_controller/process'
  #
  # get 'api_controller/map'
  #
  # get 'api_controller/process'

  devise_for :admin_users, ActiveAdmin::Devise.config

  devise_for :users, path: '/~admin~/users'

  # importable
  concern :importable do
    collection do
      get :import
      post :import
    end
  end

  scope path: '/~admin~/' do
    # devise_for :users

    resources :domain_configs, concerns: :importable do
      resources :replaces
      resources :cache_settings
      resources :subdomain_configs do
        resources :replaces
      end
    end
    # resources :subdomain_configs, only: :destroy
    # resources :replaces, only: :destroy
  end


  ActiveAdmin.routes(self)
  namespace :proxy_translator_admin do
    resources :settings

    resources :domain_configs do
      resources :subdomain_configs do
        resources :replaces
      end
    end

    resources :subdomain_configs do
      resources :replaces
    end

    resources :global_replacements do
      resources :replaces
    end
  end

  [
    ActiveAdmin::ResourceController,
    ActiveAdmin::Devise::SessionsController,
    ProxyTranslatorAdmin::DashboardController
  ].map do |skipped_class|
    skipped_class.class_eval do
      skip_authorization_check
    end
  end


  get '/~admin~', to: 'home#index', as: :admin_landing

  # root 'proxy#proxy'
  get '/:system(/*path)', to: 'proxy#system', system: /~.*?~/, format: false

  # file proxy
  get '(*path).:format' => 'proxy#file', constraints: { format: /(css|js|png|jpg|jpeg|gif|svg|json|xml|ico|txt|eot|woff|ttf)/i }

  # page translate & proxy
  get '(*path)' => 'proxy#proxy', format: false
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  match '*unmatched_route', :to => 'application#raise_not_found!', :via => :all
end
