environment 'production'
daemonize false

pidfile 'tmp/pids/puma.pid'
state_path 'tmp/pids/puma.state'
threads 4, 10
bind 'unix://tmp/sockets/proxy_translate-puma.sock'

# activate_control_app 'unix://tmp/sockets/pumactl.sock'