ProxyCache = ActiveSupport::Cache::ProxyStore.new 'tmp/proxy/'


# Clear matched path. Works with a partial name or path, domain is ok
# ProxyCache.delete_matched 'localhost/images/pogo'

# Clear all proxied resources, or domain related only
# ProxyCache.clear(domain = '')


# Cleanup obsolete items
# ProxyCache.cleanup — ToDo ?