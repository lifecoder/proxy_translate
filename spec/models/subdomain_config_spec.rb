require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SubdomainConfig do
  describe 'restricts' do
    before do
      create :domain_config, domain: 'test.example.com'
    end

    let!(:domain) { create :domain_config, domain: 'example.com' }

    it 'passes for unique subdomain' do
      expect(build :subdomain_config, subdomain: 'de', domain_config: domain).to be_valid
    end

    it 'passes on update' do
      new_subdomain = create :subdomain_config, subdomain: 'de', domain_config: domain
      expect(new_subdomain.update({})).to be_truthy
    end


    it 'subdomains when overlapped with an existing domain' do
      expect(build :subdomain_config, subdomain: 'test', domain_config: domain).to be_invalid
    end

    it 'non alphanumeric subdomains' do
      expect(build :subdomain_config, subdomain: 'test.me').to be_invalid
    end
  end


end