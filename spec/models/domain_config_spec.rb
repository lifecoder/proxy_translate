require 'rails_helper'

describe DomainConfig do
  describe 'factory' do
    it 'works' do
      expect{create :domain_config}.to change{DomainConfig.count}.by 1
    end
  end

  describe 'restricts' do
    let!(:domain) { create :domain_config, domain: 'example.com' }

    it 'passes for an unique domain' do
      expect(build :domain_config, domain: 'other.example.com').to be_valid
    end

    it 'passes on update' do
      new_domain = create :domain_config, domain: 'other.example.com'
      expect(new_domain.update({})).to be_truthy
    end

    it 'domain creation when overlaps with existing domain' do
      expect(build :domain_config, domain: 'example.com').to be_invalid
    end

    it 'domain creation when overlaps with existing subdomain.domain' do
      create :subdomain_config, domain_config: domain, subdomain: 'de'
      expect(build :domain_config, domain: 'de.example.com').to be_invalid
    end
  end

  describe '#delete' do
    let!(:domain) { create :domain_config, domain: 'example.com' }
    let!(:subdomain) { create :subdomain_config, domain_config: domain, subdomain: 'de' }

    it 'should delete subdomain configs' do
      expect{ domain.destroy }.to change(SubdomainConfig, :count).by(-1)
    end
  end
end