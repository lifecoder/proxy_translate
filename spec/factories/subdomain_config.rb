FactoryGirl.define do
  factory :subdomain_config do
    domain_config { build(:domain_config) }
    result_lang_code 'de'
    subdomain 'de'
  end
end
