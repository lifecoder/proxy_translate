require 'active_support/core_ext/marshal'
require 'active_support/core_ext/file/atomic'
require 'active_support/core_ext/string/conversions'
require 'uri/common'

module ActiveSupport
  module Cache
    # A cache store implementation which stores everything on the filesystem.
    #
    # ProxyStore is a FileStore ancestor implementing
    # semantic Nginx-friendly file storage
    class ProxyStore < FileStore
      # Deletes all items from the cache. In this case it deletes all the entries in the specified
      # file store directory except for .gitkeep. Be careful which directory is specified in your
      # config file when using +FileStore+ because everything in that directory will be deleted.
      def clear(domain = '')
        target_path = File.join cache_path, domain
        root_dirs = Dir.entries(target_path).reject {|f| (EXCLUDED_DIRS + ['.gitkeep']).include?(f)}
        FileUtils.rm_r(root_dirs.collect{|f| File.join(target_path, f)})
      end

      # ToDo: own cleanup may be required
      # Preemptively iterates through all stored keys and removes the ones which have expired.
      def cleanup(options = nil)
        throw 'Check the next lines work or re-implement!'
        options = merged_options(options)
        search_dir(cache_path) do |fname|
          key = file_path_key(fname)
          entry = read_entry(key, options)
          delete_entry(key, options) if entry && entry.expired?
        end
      end

      protected

      def read_entry(key, options)
        file_name = key_file_path(key)
        if File.exist?(file_name)
          ActiveSupport::Cache::Entry.new(
            {
                data: File.open(file_name) { |f| f.read },
                content_type: MIME::Types.type_for(file_name).first.content_type
            }
          )
        end
      rescue => e
        logger.error("FileStoreError (#{e}): #{e.message}") if logger
        nil
      end

      def write_entry(key, entry, options)
        # byebug
        # p (entry.inspect)
        file_name = key_file_path(key)
        return false if options[:unless_exist] && File.exist?(file_name)
        ensure_cache_path(File.dirname(file_name))
        File.atomic_write(file_name, cache_path) {|f| f.write(entry.value[:data]) }
        true
      end

      private

      # Translate a key into a file path.
      def key_file_path(key)
        File.join(cache_path, key)
      end

      # Translate a file path into a key.
      def file_path_key(path)
        fname = path[cache_path.to_s.size..-1]
        fname
      end
    end
  end
end