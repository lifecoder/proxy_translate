class ExpectedError < StandardError
  attr_accessor :status
  def initialize(status=404)
    super
    self.status = status
  end
end