class DbCache
  include Mongoid::Document
  include Mongoid::Timestamps::Created::Short

  field :domain, type: String # cleanup criteria
  field :domain_path, type: String # search criteria
  # %w(clean final)
  field :data, type: String
  field :type, type: String

  # def self.setClean(data)
  #   self.create data.merge({type: 'clean'})
  # end
  #
  # def self.setFinal(data)
  #   self.create data.merge({type: 'final'})
  # end

  singleton_class.send :alias_method, :clear, :delete_all

  def self.fetch domain_config, path, type
    locator = {
        domain: domain_config.domain,
        domain_path: "#{domain_config.full_domain}#{path}",
        type: type
    }
    entry = self.where(domain_path: locator[:domain_path], type: type).first
    return entry.data unless entry.nil?

    data = yield
    self.create locator.merge({ data: data })
    return data || ''

  # rescue StandardError => e
  #   Rails.logger.error("DbCache failed with: #{e.message}\n#{e.backtrace.join("\n")}")
  #   yield
  end
end