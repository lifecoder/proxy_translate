class LocalAsset
  attr_reader :path, :config

  UPLOADS_PATH = 'uploads'

  def initialize(path, domain)
    @config = DomainConfig.find_by! domain: domain
    @path = path
  end

  def get
    asset_path = Rails.root.join('public', UPLOADS_PATH, config.domain, path.sub(/^\//, '')).to_s
    if File.exist?(asset_path) && File.file?(asset_path)
      { static: true, path: asset_path}
    else
      false
    end
  end

end