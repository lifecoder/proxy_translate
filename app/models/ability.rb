class Ability
  include CanCan::Ability

  def initialize(user)
    # can :manage, :all
    return unless user

    user_abilities(user) if user.is_a?(User)

    admin_user_abilities(user) if user.is_a?(AdminUser)

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end

  def user_abilities user
    can :manage, DomainConfig, :user_id => user.id

    can :manage, SubdomainConfig, domain_config: { :user_id => user.id }
    can :create, SubdomainConfig

    can :manage, CacheSetting, domain_config: { :user_id => user.id }
    can :create, CacheSetting

    can :manage, Replace
    # can :manage, Replace, domain_config: { :user_id => user.id }
    # can :create, Replace
  end

  def admin_user_abilities user
    can :manage, :all
  end
end
