##
# RegEx note: /s in PHP equals to /m in Ruby
##
#ToDo: wrap into Engine module
class Google
  # COMMON — to take out if multiple engines present
  CURL_CONF = {
      support_timeout: 1,
      operation_timeout: 10,
      max_redirects: 5
  }

  TRANSLATION_URL = 'http://translate.google.com/translate?hl=en&sl=%{source_lang_code}&tl=%{result_lang_code}&u=%{url}&prev=hp'
  attr_reader :content, :sanitized_content, :depth, :entry_url, :final_url, :config, :normal
  MAX_REDIRECT_DEPTH = 5

  # codes to config model
  def initialize(request_config, html = nil) #, path = nil)#, domain)
    #ToDo: fetch cache
    @config = request_config
    @path = request_config.path

    if html
      @content = html
    else
      raise ExpectedError.new(404), 'No Donor URL' if @config[:donor_url].empty?
      begin
        @final_url = @entry_url = @url = TRANSLATION_URL % {url: URI.join(@config[:donor_url], @path),
                                                            source_lang_code: @config[:source_lang_code],
                                                            result_lang_code: @config[:result_lang_code]}
      rescue => e
        Rails.logger.error "Join failed for #{@config[:donor_url]} :: #{@path}\nRequest Config: #{@config.inspect}"
        raise e
      end

      Rails.logger.debug "URL: #{@url}"

      if @config.development_mode?
        @content = fetch @url
      else
        @content = DbCache.fetch(@config, @path, 'clean') do
          fetch @url
        end
      end
    end



    sanitize!

    replace!
  end

  def raw
    @content
  end

  def clean
    @sanitized_content
    # doc = Nokogiri::HTML(@content)
    # doc.to_html
  end

  def to_s
    clean
  end

  # CheatSheet
  # result = str.scan(/some(regex)here/i)
  # result = str.gsub(/some(regex)here/, 'replace_str')

  def fetch(url)
    # we may use a cache to store raw value
    # and clear only sanitized data for pattern updates
    @depth = 0
    load_and_translate url
  end

private

  def load_and_translate(url)
    @depth +=1
    throw 'Max redirect depth reached!' if @depth > MAX_REDIRECT_DEPTH

    proxy = Setting.proxy

    proxy = proxy.nil? ? nil : "socks5://#{proxy}"
    begin
      html = fetch_url url, proxy
    rescue => e
      Rails.logger.debug "ERR:Rescued>> Rescued: (proxy=#{proxy.inspect}) fetch failed with: #{e}"
      html = fetch_url(url, nil) unless proxy.nil?
    end

    raise ExpectedError.new(503), 'Content unavailable' if html.nil?

    html = html.force_encoding('UTF-8')
    result = check(html)
    #ToDo: handle non-OK check results
    if result[:redirect]
      @final_url = result[:redirect]
      fetch(@final_url)
    else
      return html
    end
  end

  def fetch_url(url, proxy)
    c = Curl::Easy.new(url) do |curl|
      curl.proxy_url = proxy
      # curl.proxypwd = user
      curl.follow_location = true
      # timeouts here are integers _only_!
      # * docs has ms-timed functions but they are not accessible here (different version?)
      # see: http://www.rubydoc.info/github/taf2/curb/Curl/Easy#connect_timeout_ms-instance_method
      curl.connect_timeout = self.class::CURL_CONF[:support_timeout]
      curl.dns_cache_timeout = self.class::CURL_CONF[:support_timeout]
      curl.timeout = self.class::CURL_CONF[:operation_timeout]
      # curl.verbose = true
      curl.max_redirects = self.class::CURL_CONF[:max_redirects]
      curl.headers['User-Agent'] = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)'
    end

    c.perform

    c.body_str
  end


  def check(html)
    # redirect check
    result = html.scan(%r{<iframe sandbox="allow-same-origin allow-forms allow-scripts" src="(http://translate.googleusercontent.com/translate_p\?[^"]+)}).first.first rescue nil
    return { redirect: result.gsub(/&amp;/, '&') } if result

    result = html.scan(%r{<meta http-equiv="refresh" content="0;URL=([^"]+)}).first.first rescue nil
    return { redirect: result.gsub(/&amp;/, '&') } if result

    return { status: :ok }
  end

  def sanitize!
    @sanitized_content = @content.clone
    normalize
    apply_global_replacements

    remove_iframe
    remove_onload
    remove_css
    remove_scripts
    remove_span_wrappers

    tag_fixup

    default_fixup
  end

  # Apply DOM manipulator normalization by parse-save content
  #
  # This may cause breaking changes for incorrect document structure
  # so we want to do this as early as possible to be able to fix some
  # known issues within global replacements application
  def normalize
    doc = Nokogiri::HTML(@sanitized_content)
    @sanitized_content = doc.to_html

    @normal = @sanitized_content.dup
  end

  def apply_global_replacements
    GlobalReplacement::global.replaces.each do|replace|
      @sanitized_content.gsub! replace.pattern.to_regexp, replace.replacement
    end
  end

  # ToDo: move to commons
  def replace!
    @config.replaces.each do |replace|
      @sanitized_content.gsub!(replace.pattern.to_regexp, replace.replacement) rescue ''
    end
  end

  def default_fixup
    # remove <base> tag _if_ we need it
    # @sanitized_content.gsub! %r{<base href=[^\>]*?\>}i, ''
    doc = Nokogiri::HTML(@sanitized_content)
    doc.css('base').each { |fragment| fragment.remove }

    # decode uri's
    # doc.css('a').each do |fragment|
    # possible NoMethodError (undefined method `value' for nil:NilClass):
      # href = fragment.attributes['href'].value
      # fragment.attributes['href'].value = URI.decode href
    # end

    @sanitized_content = doc.to_html

    # route all links to the proxy domain

    # proxy_domain = URI.parse(Setting.get('proxy_site_url')).host
    target_domain = URI.parse(@config[:donor_url]).host
    @sanitized_content.gsub! %r{<a[^>]*href=[^>]*(#{target_domain})[^>]*>} do |href|
      href.gsub! %r{https?://#{target_domain}}, config.proxy_site_url
      # href.gsub!(%r{href=[ ''](.*)[ '"]}){ |a,b| URI.join(@path, a).to_s }
    end
    # WARNING: broke document due to previously bringed issues or something,
    # document.to_html brings valid but different html

    # document = Nokogiri::HTML @sanitized_content
    # links = document.css('a')
    # links.each { |link| link['href'] = link['href'].gsub(%r{https?://#{Setting.get('target_site_url').sub(/^https?\:\/\//, '')}}, proxy_url) if link['href'] }
    # @sanitized_content = document.to_html
  end

  def remove_onload
    @sanitized_content.gsub! %r{onload="[^"]*?"}i, ''
  end

  def extract_orig_url(url='')
    #ToDo -- check
    match = nil
    if url.include? 'translate_c'
      url.gsub!(/.*u=/i, '')
      match = url.scan(/#[^ ">]*/).first
      url.gsub! /\&amp;.*/i, ''
    else
      if url.include? 'translate_un'
        url.gsub!(/.*u=/i, '')
        match = url.scan(/#[^ ">]*/).first
        url.gsub! /\&prev.*/i, ''
      end
    end
    # puts ">> #{url} || #{match.inspect}"
    #ToDo: possibly incorrect `match`
    !match.nil? ? CGI.unescape(url+match) : url
  end

  def fix_link_cb(m)
    finish = m.last
    extract_orig_url(m) + finish
  end


  def fix_tag_links
    # php> $content = preg_replace_callback($rstr,'fix_link_cb',$content);
    @sanitized_content.gsub!(%r{(https?://[^">]+?/translate_[^">]*?)([ ">])}) { |link| fix_link_cb(link) }
  end

  def tag_fixup
    remove_onload
    fix_tag_links
  end

  def remove_iframe
    @sanitized_content.gsub! %r{<iframe[^>]*?translate\.google\.com[^>]*?</iframe>}im, ''
  end

  def remove_scripts
    [
       %r{<script src="http://(\S.*?)translate_c[^<]*?</script>},
       %r{<script>.*?_intlStrings[^<]*?</script>},
       %r{<script>.*?function ti_\(\)[^<]*?</script>},
       %r{<script>.*?_setupIW\(\)[^<]*?</script>},
       %r{<script>[^<]*performance[^<]*</script>},
    ].each do |pattern|
      @sanitized_content.gsub! pattern, ''
    end
  end

  def remove_css
    @sanitized_content.gsub! %r{<style type="text/css">\.google-src-text[^<]*?</style>}, ''
  end

  def remove_span_wrappers
    # What was `' . '`? >>php>> $mstr = '#<span class="google-src-text".*?' . '>(.*?)</span>#is';
    doc = Nokogiri::HTML(@sanitized_content)
    doc.css('.google-src-text').each { |fragment| fragment.remove }
    @sanitized_content = doc.to_html
    # @sanitized_content.gsub! %r{<span class="google-src-text".*?>(<span.*?>.*?<\/span>)*(.*?)</span>}im, ''
    @sanitized_content.gsub! %r{<span class="notranslate" onmouseover="_tipon\(this\)" onmouseout="_tipoff\(\)">(.*?)</span>}im, '\1'
  end
end