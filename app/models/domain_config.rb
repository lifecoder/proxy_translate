class DomainConfig < ActiveRecord::Base
  has_many :replaces, as: :replaceable
  has_many :cache_settings
  has_many :subdomain_configs, dependent: :destroy
  belongs_to :user

  validates_with DomainUniquenessValidator, on: :create
  validates :donor_url, uniqueness: true
  before_save :normalize_donor_url

  def to_s
    domain
  end

  def subdomains_count
    subdomain_configs.count
  end

  def normalize_donor_url
    url = self.donor_url
    url.strip!
    unless url.empty? || url[/\Ahttps?:\/\//]
      url = "http://#{url}"
    end

    uri = URI.parse(url)
    self.donor_url = "#{uri.scheme}://#{uri.host}"
  end

  def domain_link
    "http://#{domain}"
  end

end
