class DomainConfigManager < ActiveRecord::Base
  class << self
    # Load configuration for the specified domain
    #
    # Throw exception if no associated configuration available
    def load!(domain, path='')
      result = test_and_return_configuration(domain, path)
      unless result[:present]
        Rails.logger.debug "Config not found for #{domain}"
        raise ExpectedError.new(404), 'Domain config not found'
      end
      result
    end

    # Test if a configuration for the domain is present in DB
    def test(domain, path='')
      test_and_return_configuration(domain, path)[:present]
    end

  private

    def test_and_return_configuration(domain, full_path)
      # try to load as a domain
      subdomain_config = nil
      domain_config = load_domain_config domain
      path = full_path
      subdomain_mode = :subdomain

      # no domain found — try to find a subdomain+domain pair
      if domain_config.nil?
        subdomain, domain = domain.split('.', 2)
        domain_config = load_domain_config domain
        subdomain_config = load_subdomain_config(domain_config, subdomain) if domain_config
      else
        # no subdomain => checking for path part
        # if subdomain.nil?
        _tmp, subdomain, real_path =  full_path.split('/', 3)
        subdomain_config = load_subdomain_config(domain_config, subdomain) if domain_config
        if subdomain_config.present?
          subdomain_mode = :folder
          path = "/#{real_path}"
        else
          subdomain = nil
        end
      end

      test = domain_config.present? && (subdomain.blank? || subdomain_config.present?)

      {
          present: test,
          domain: domain,
          subdomain: subdomain,
          subdomain_mode: subdomain_mode,
          domain_config: domain_config,
          subdomain_config: subdomain_config,
          path: path
      }
    end

    def load_domain_config(domain)
      DomainConfig.find_by(domain: domain)
    end

    def load_subdomain_config(domain_config, subdomain)
      SubdomainConfig.find_by(domain_config_id: domain_config.id,
                              subdomain: subdomain)
    end
  end
end
