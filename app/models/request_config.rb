class RequestConfig
  attr_reader :domain, :subdomain, :path, :domain_config, :subdomain_config

  def initialize(domain, path='')
    @domain, @subdomain, @domain_config, @subdomain_config, @subdomain_mode, @path =
        DomainConfigManager
            .load!(domain, path)
            .values_at(:domain, :subdomain, :domain_config, :subdomain_config, :subdomain_mode, :path)
  end

  def params
    @params ||= domain_config.attributes.merge subdomain_config_attributes
  end

  def [] param_name
    params[param_name.to_s]
  end

  def proxy_site_url
    domain_link
  end

  def cache_time_for(path)
    @domain_config.cache_settings.all.reverse_each do |rule|
      return rule.expire_after_seconds if File.fnmatch(rule.route_mask, path)
    end

    CacheSetting::DEFAULT_CACHE_TIME
  end

  def development_mode?
    # ToDo: serialize and custom form — and no cluttering with checks here
    %w(1 on true).include? [:development_mode].to_s.downcase
  end

  def root_domain
    @domain
  end

  def full_domain
    domain = @domain
    # @subdomain ? [@subdomain, @domain].join('.') : @domain
    if @subdomain.present?
      domain = @subdomain_mode == :subdomain ?
          [@subdomain, @domain].join('.') :
          [@domain, @subdomain].join('/')
    end

    domain
  end

  def domain_link
    "http://#{full_domain}"
  end

  # TEMP: until better multilanguage config
  def replaces
    replaces = domain_config.replaces.to_a
    replaces += subdomain_config.replaces.to_a if subdomain_config
    replaces
  end

  def cache_key
    # OpenStruct.new(
    #   {
    #       # subdomain: subdomain, # should not be used yet
    #       domain: domain,
    #       path: path
    #   }
    # )

    # has to be a string
    File.join domain, path
  end

private
  def subdomain_config_attributes
    subdomain_config ? subdomain_config.attributes : {}
  end
end