class AssetsProxy
  attr_reader :path, :url, :config

  UPLOADS_PATH = 'uploads'

  CURL_CONF = {
      support_timeout: 1,
      operation_timeout: 10,
      max_redirects: 5
  }

  def initialize(request_config)
    @request_config = request_config
    @config = DomainConfig.find_by! domain: request_config.domain
    @path = request_config.path
    @url = URI.join(config.donor_url, path).to_s
  end

  def get
    file = local_asset
    return file if file

    # @content = ProxyCache.fetch(@request_config, expires_in: CacheSetting::DEFAULT_ASSETS_CACHE_TIME) do
    fetch
    # end
  end

private

  def fetch
    # to extract into rescuable proxy module or class
    proxy = Setting.proxy

    proxy = proxy.nil? ? nil : "socks5://#{proxy}"

    asset = fetch_url proxy

    http_response, *http_headers = asset.header_str.split(/[\r\n]+/).map(&:strip)
    http_headers = Hash[http_headers.flat_map{ |s| s.scan(/^(\S+): (.+)/) }]

    data = asset.body_str

    {
      data: data,
      content_type: http_headers['Content-Type']
    }
  end

  def fetch_url(proxy)
    c = Curl::Easy.new(url) do |curl|
      curl.proxy_url = proxy
      # curl.proxypwd = user
      curl.follow_location = true
      # timeouts here are integers _only_!
      # * docs has ms-timed functions but they are not accessible here (different version?)
      # see: http://www.rubydoc.info/github/taf2/curb/Curl/Easy#connect_timeout_ms-instance_method
      curl.connect_timeout = self.class::CURL_CONF[:support_timeout]
      curl.dns_cache_timeout = self.class::CURL_CONF[:support_timeout]
      curl.timeout = self.class::CURL_CONF[:operation_timeout]
      # curl.verbose = true
      curl.max_redirects = self.class::CURL_CONF[:max_redirects]
      curl.headers['User-Agent'] = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)'
    end

    c.perform

    c
  end

  def local_asset
    asset_path = Rails.root.join('public', UPLOADS_PATH, config.domain, path.sub(/^\//, '')).to_s
    if File.exist?(asset_path) && File.file?(asset_path)
      { static: true, path: asset_path}
    else
      false
    end
  end
end