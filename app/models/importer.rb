class Importer
  include ActiveModel::Model
  include ActiveModel::Validations
  attr_accessor :file, :token, :importable

  validates :file, presence: true

  def initialize(params=nil, importable={})
    throw('No matchers provided!') unless importable[:matchers].present?
    importable.reverse_merge! data: {}

    self.importable = importable
    super(params)
  end

  def process
    results = { total: 0, success: 0, failed: 0, failed_items: [] }

    SmarterCSV.process(file.path, key_mapping: import_key_mapping, remove_unmapped_keys: true) do |items|
      row = items.first
      row.merge! importable[:data]
      matchers = matchers(row)
      item = importable[:resource_class].find_or_initialize_by matchers
      if item.update(row)
        results[:success] += 1
      else
        results[:failed] += 1
        results[:failed_items].push({data: matchers, reasons: item.errors.messages})
      end
    end

    results
  end

private

  def matchers(row)
    matchers = {}
    importable[:matchers].each do |k, v|
      matchers[k] = v.is_a?(Symbol) ? row[v] : v
    end

    matchers
  end

  def import_key_mapping
    mapping = {}
    importable[:fields].each do |f|
      mapping[f] = f
    end

    mapping
  end
end