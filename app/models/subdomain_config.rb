class SubdomainConfig < ActiveRecord::Base
  belongs_to :domain_config
  has_many :replaces, as: :replaceable

  validates_with DomainUniquenessValidator, on: :create
  validates :subdomain, format: { with: /\A[a-zA-Z]+\z/,
                                  message: I18n.t('subdomain.format') }

  def domain
    "#{subdomain}.#{domain_config.domain}"
  end

  delegate :user, to: :domain_config
end
