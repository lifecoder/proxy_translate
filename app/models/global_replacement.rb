class GlobalReplacement < ActiveRecord::Base
  has_many :replaces, as: :replaceable

  GLOBAL = 'global'

  def self.global
    self.find_by_name GLOBAL
  end
end
