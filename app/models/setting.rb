class Setting < ActiveRecord::Base
  def self.get(key)
    self.where(key: key.to_s).first[:value]
  end

  def self.proxy
    @proxy ||= self.get(:proxy).split(/\s+/).sample
  end

  def self.development_mode
    # ToDo: serialize and custom form — and no cluttering with checks here
    %w(1 on true).include? self.get(:development_mode)
  end
end
