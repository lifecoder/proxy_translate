class Replace < ActiveRecord::Base
  belongs_to :replaceable, polymorphic: true

  validates :replaceable_id, presence: true

  scope :by_weight, ->{ order id: :desc }
end