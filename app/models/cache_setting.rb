class CacheSetting < ActiveRecord::Base
  belongs_to :domain_config

  # default cache time in seconds
  DEFAULT_CACHE_TIME = 1.hour
  DEFAULT_ASSETS_CACHE_TIME = 1.month

  def expire_after= (val)
    write_attribute :expire_after, ChronicDuration.parse(val, keep_zero: true)
  end

  def expire_after
    ChronicDuration.output read_attribute(:expire_after) || 0, format: :short, keep_zero: true
  end

  def expire_after_seconds
    read_attribute(:expire_after) || 0
  end
end
