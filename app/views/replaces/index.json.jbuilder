json.array!(@replaces) do |replace|
  json.extract! replace, :id, :pattern, :replacement
  json.url replace_url(replace, format: :json)
end
