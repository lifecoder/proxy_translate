json.array!(@domain_configs) do |domain_config|
  json.extract! domain_config, :id, :domain, :donor_url, :source_lang_code, :result_lang_code, :development_mode
  json.url domain_config_url(domain_config, format: :json)
end
