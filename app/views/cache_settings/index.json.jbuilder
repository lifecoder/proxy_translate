json.array!(@cache_settings) do |cache_setting|
  json.extract! cache_setting, :id, :route_mask, :expire_after
  json.url cache_setting_url(cache_setting, format: :json)
end
