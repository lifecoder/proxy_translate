json.array!(@subdomain_configs) do |subdomain_config|
  json.extract! subdomain_config, :id, :domain_config_id, :subdomain, :result_lang_code
  json.url subdomain_config_url(subdomain_config, format: :json)
end
