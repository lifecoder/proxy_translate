class DomainUniquenessValidator < ActiveModel::Validator
  def validate(record)
    record.errors[:base] << I18n.t('domains.duplicate') if DomainConfigManager.test(record.domain)
  end
end