ActiveAdmin.register Replace do
  # ToDo: automatically filter out id and tie with nested resource
  # Warning: it SHOULD include domain_config_id
  #          and SHOULD NOT include id
  active_admin_import #before_batch_import: ->(importer) {
                      #  abort
                      #}

  belongs_to :global_replacement, polymorphic: true, optional: false
  belongs_to :subdomain_config, polymorphic: true, optional: true
  belongs_to :domain_config, polymorphic: true, optional: true

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :pattern, :replacement
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
