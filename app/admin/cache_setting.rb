ActiveAdmin.register CacheSetting do
  belongs_to :domain_config
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :route_mask, :expire_after
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  form do |f|
    f.inputs do
      f.input :route_mask
      f.input :expire_after, as: :string
    end
    f.actions
  end
end
