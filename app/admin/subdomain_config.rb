ActiveAdmin.register SubdomainConfig do
  permit_params :subdomain, :result_lang_code

  belongs_to :domain_config, optional: true

  index do
    # column (:subdomain) { |config| link_to config.subdomain, config.domain_link, target: '_blank' }
    # column (:donor_url) { |config| link_to config.donor_url, config.donor_url, target: '_blank' }
    # column :source_lang_code
    # column :result_lang_code

    column :subdomain
    # column :development_mode
    actions
    actions defaults: false do |config|
      # link_to 'Replacements', proxy_translator_admin_domain_config_subdomain_config_replaces_path(config.domain_config.id, config.id)
      link_to 'Replacements', proxy_translator_admin_subdomain_config_replaces_path(config.id)
    end
  end

  csv do
    column :subdomain
    column :result_lang_code
    column :domain
    column :user
  end

end
