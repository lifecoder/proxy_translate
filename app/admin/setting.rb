ActiveAdmin.register Setting do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :key, :value
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  collection_action :clear_cache do
    # Rails.cache.clear
    DbCache.clear
    # ProxyCache.clear ''
    redirect_to collection_path, notice: 'Cache cleared'
  end


  action_item :add do
    link_to 'Clear cache', clear_cache_proxy_translator_admin_settings_path
  end

end
