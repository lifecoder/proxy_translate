ActiveAdmin.register DomainConfig do
  # ToDo nested resources Replaces
  permit_params :domain, :donor_url, :source_lang_code, :result_lang_code, :development_mode, :user_id

  index do
    column (:domain) { |config| link_to config.domain, config.domain_link, target: '_blank' }
    column (:donor_url) { |config| link_to config.donor_url, config.donor_url, target: '_blank' }
    column :source_lang_code
    column :result_lang_code
    column :subdomains_count
    column :user
    actions
    actions defaults: false do |config|
      link_to 'Replacements', proxy_translator_admin_domain_config_replaces_path(config.id)
    end
    actions defaults: false do |config|
      link_to 'Cache Settings', proxy_translator_admin_domain_config_cache_settings_path(config.id)
    end
    actions defaults: false do |config|
      link_to 'Subdomains', proxy_translator_admin_domain_config_subdomain_configs_path(config.id)
    end
  end

  csv do
    column :domain
    column :donor_url
    column :source_lang_code
    column :result_lang_code
    column :subdomains_count
    column :user
  end
end
