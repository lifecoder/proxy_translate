ActiveAdmin.register GlobalReplacement do
  permit_params :name

  index do
    column :name
    actions defaults: false do |config|
      link_to 'Replacements', proxy_translator_admin_global_replacement_replaces_path(config.id)
    end
    actions
  end
end
