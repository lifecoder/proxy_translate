class ReplacesController < ApplicationController
  load_resource :domain_config
  load_resource :subdomain_config
  load_and_authorize_resource through: [:subdomain_config, :domain_config]

  def index
    @replaces = @replaces.by_weight
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    respond_to do |format|
      if @replace.save
        format.html { redirect_to [@domain_config, @subdomain_config, @replace].reject(&:blank?), notice: 'Replace was successfully created.' }
        format.json { render :show, status: :created, location: @replace }
      else
        format.html { render :new }
        format.json { render json: @replace.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @replace.update(replace_params)
        format.html { redirect_to [@domain_config, @subdomain_config, @replace].reject(&:blank?), notice: 'Replace was successfully updated.' }
        format.json { render :show, status: :ok, location: @replace }
      else
        format.html { render :edit }
        format.json { render json: @replace.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @replace.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Replace was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def replace_params
      params.require(:replace).permit(:pattern, :replacement)
    end
end
