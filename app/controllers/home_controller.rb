class HomeController < ApplicationController
  skip_authorization_check

  def index
    if current_user
      redirect_to domain_configs_path
    else
      redirect_to new_user_session_path
    end
  end
end