# adds an import ability to a resource
module Importable
  extend ActiveSupport::Concern

  def import
    @importable = importable
    case request.method
      when 'GET'
        import_landing
      when 'POST'
        import_create
      else
        raise ActionController::RoutingError.new('Not Found')
    end
  end

private

  def import_landing
    @importer = Importer.new(nil, importable)
  end

  def import_create
    @importer = Importer.new(importer_params, importable)

    if @importer.valid?
      @results = @importer.process
      # redirect_to :back, notice: "Import done. Items updated: #{result[:success]}. Failed rows: #{result[:failed]}"
      # render 'import_results'
    else
      render :import
    end
  end

  def importer_params
    params.require(:importer).permit(:file)
  end

  # provides info about resource import handling
  #
  # @return Hash resource import info
  def importable
    throw 'No importable info provided!'
    {
        resource_class: SubdomainConfig,            # Resource model class
        ### # parent_class: DomainConfig,
        # parent: @domain_config,
        fields: [ :subdomain ],                     # Array of importable fields
        matchers: {                                 # Set of matchers to check if this item exists and update if it is
            user_id: current_user.id,               #   raw values would be used as is
            domain: :domain                         #   symbols reference to a row field to get value from
        },
        data: {                                     # Data to merge with the rows provided
            user_id: current_user.id,
        },
        transform: ->(row) {                        # Transformer for the data provided (ToDo: concept, not used)
          row[:email] = row[:e] + row[:mail]
          row.delete(:first) ; row.delete(:last)
        }
    }
  end
end