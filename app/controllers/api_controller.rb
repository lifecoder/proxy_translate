class ApiController < ApplicationController
  skip_authorization_check

  def map
    map = {}
    DomainConfig.all.each do |conf|
      if request[:extra]
        subdomains = conf.subdomain_configs.pluck(:subdomain, :result_lang_code).push([nil, conf.result_lang_code])
        map[conf.domain] = {
          targetUrl: conf.donor_url,
          subdomains: subdomains.map { |item| Hash[[:name, :targetLangCode].zip item] }
        }
      else
        map[conf.domain] = conf.donor_url
      end
    end

    render json: map
  end

  def xprocess
    host = [ params[:domain], params[:subdomain] ].compact.join('.')

    config = RequestConfig.new(host)
    render json: { html: Google.new(config, params[:html]).clean }
  end
end
