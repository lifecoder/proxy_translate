class ApplicationController < ActionController::Base
  check_authorization unless: :devise_controller?

  require 'sys/filesystem'
  include Sys
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_filter :cache_cleanup


  before_action do
    # byebug
    # if current_admin_user
    #   Rack::MiniProfiler.authorize_request
    # end
  end

  # def access_denied(exception)
  #   redirect_to admin_path, :alert => exception.message
  # end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to admin_landing_path, :alert => exception.message
  end

  rescue_from ExpectedError do |e|
    render nothing: true, status: e.status
  end


  rescue_from Curl::Err::TimeoutError do |e|
    render nothing: true, status: 500
  end

  #called by last route matching unmatched routes.  Raises RoutingError which will be rescued from in the same way as other exceptions.
  def raise_not_found!
    raise ActionController::RoutingError.new("No route matches #{params[:unmatched_route]}")
  end

  unless Rails.application.config.consider_all_requests_local
    rescue_from ActionController::RoutingError do |e|
      Rails.logger.debug("Routing error: #{e}")
      render nothing: true, status: 404
    end
  end


  protected

  def after_sign_in_path_for(resource)
    request.env['omniauth.origin'] || stored_location_for(resource) || admin_landing_path
  end

  def after_sign_out_path_for(resource_or_scope)
    admin_landing_path
  end


  def cache_cleanup
    stat = Filesystem.stat('/')
    free_mem = stat.block_size * stat.blocks_available
    bottom_limit = Rails.application.config.panic_when_space_left
    if free_mem < bottom_limit
      # Rails.cache.clear
      DbCache.clear
      # ProxyCache.clear ''
    end

    # Rails.cache.cleanup if rand(10) > 7
  end

  # def devise_parameter_sanitizer
  #   if resource_class == User
  #     User::ParameterSanitizer.new(User, :user, params)
  #   else
  #     super # Use the default one
  #   end
  # end
end