class DomainConfigsController < ApplicationController
  # before_action :set_domain_config, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  include Importable

  # GET /domain_configs
  # GET /domain_configs.json
  def index
    # @domain_configs = DomainConfig.all
  end

  # GET /domain_configs/1
  # GET /domain_configs/1.json
  def show
  end

  # GET /domain_configs/new
  def new
    # @domain_config = DomainConfig.new
  end

  # GET /domain_configs/1/edit
  def edit
  end

  # POST /domain_configs
  # POST /domain_configs.json
  def create
    # @domain_config = DomainConfig.new(domain_config_params)

    @domain_config.user_id = current_user.id

    respond_to do |format|
      if @domain_config.save
        format.html { redirect_to @domain_config, notice: 'Domain config was successfully created.' }
        format.json { render :show, status: :created, location: @domain_config }
      else
        format.html { render :new }
        format.json { render json: @domain_config.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /domain_configs/1
  # PATCH/PUT /domain_configs/1.json
  def update
    respond_to do |format|
      if @domain_config.update(domain_config_params)
        format.html { redirect_to @domain_config, notice: 'Domain config was successfully updated.' }
        format.json { render :show, status: :ok, location: @domain_config }
      else
        format.html { render :edit }
        format.json { render json: @domain_config.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /domain_configs/1
  # DELETE /domain_configs/1.json
  def destroy
    @domain_config.destroy
    respond_to do |format|
      format.html { redirect_to domain_configs_url, notice: 'Domain config was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_domain_config
      @domain_config = DomainConfig.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def domain_config_params
      params.require(:domain_config).permit(:domain, :donor_url, :source_lang_code, :result_lang_code, :development_mode)
    end

    def importable
      {
          resource_class: DomainConfig,
          fields: [ :domain, :donor_url, :source_lang_code, :result_lang_code ],
          matchers: {
              user_id: current_user.id,
              domain: :domain
          }
      }
    end
end
