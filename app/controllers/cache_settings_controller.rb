class CacheSettingsController < ApplicationController
  load_and_authorize_resource :domain_config
  load_and_authorize_resource through: :domain_config

  # GET /cache_settings
  # GET /cache_settings.json
  def index
  end

  # GET /cache_settings/1
  # GET /cache_settings/1.json
  def show
  end

  # GET /cache_settings/new
  def new
  end

  # GET /cache_settings/1/edit
  def edit
  end

  # POST /cache_settings
  # POST /cache_settings.json
  def create
    respond_to do |format|
      if @cache_setting.save
        format.html { redirect_to [@domain_config, @cache_setting], notice: 'Cache setting was successfully created.' }
        format.json { render :show, status: :created, location: @cache_setting }
      else
        format.html { render :new }
        format.json { render json: @cache_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cache_settings/1
  # PATCH/PUT /cache_settings/1.json
  def update
    respond_to do |format|
      if @cache_setting.update(cache_setting_params)
        format.html { redirect_to [@domain_config, @cache_setting], notice: 'Cache setting was successfully updated.' }
        format.json { render :show, status: :ok, location: @cache_setting }
      else
        format.html { render :edit }
        format.json { render json: @cache_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cache_settings/1
  # DELETE /cache_settings/1.json
  def destroy
    @cache_setting.destroy
    respond_to do |format|
      format.html { redirect_to domain_config_cache_settings_url, notice: 'Cache setting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def cache_setting_params
      params.require(:cache_setting).permit(:route_mask, :expire_after)
    end
end
