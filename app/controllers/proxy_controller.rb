class ProxyController < ApplicationController
  skip_authorization_check
  before_action :request_config
  before_action :local_asset

  def proxy
    if @local_asset
      send_file @local_asset[:path], type: MIME::Types.type_for(@local_asset[:path]).first.content_type, disposition: 'inline'
      return
    end

    html = get_page
    render text: html.clean
  end

  def file
    asset = AssetsProxy.new(@request_config).get

    response.headers['Expires'] = CacheSetting::DEFAULT_ASSETS_CACHE_TIME.from_now.httpdate
    if asset[:static]
      send_file asset[:path], type: MIME::Types.type_for(asset[:path]).first.content_type, disposition: 'inline'
    else
      send_data asset[:data], type: asset[:content_type], disposition: 'inline'
    end
  end

  def system
    system = params[:system]
    system.gsub! /^~(.*)~$/, '\1'

    case system
      when 'raw'
        html = get_page
        render inline: html.raw
      when 'normal'
        html = get_page
        render inline: html.normal
      when 'google:entry'
        html = get_page
        render inline: "<a href='#{html.entry_url}'>Entry URL</a>"
      when 'google:final'
        html = get_page
        render inline: "<a href='#{html.final_url}'>Final URL</a>"
      else
        raise ActionController::RoutingError.new('Not Found')
    end
  end

  private

  def local_asset
    @local_asset = LocalAsset.new(@request_config.path, @request_config.domain).get
  end

  def get_page
    Google.new @request_config
  end

  def request_config
    if params[:system]
      system = params[:system]
      path = request.env['REQUEST_URI'].sub /^\/#{system}/, ''
    else
      path = request.env['REQUEST_URI']
    end

    @request_config = RequestConfig.new(request.host, path)
  end
end