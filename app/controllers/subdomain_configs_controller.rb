class SubdomainConfigsController < ApplicationController
  load_and_authorize_resource :domain_config
  load_and_authorize_resource through: :domain_config
  # before_action :set_subdomain_config, only: [:show, :edit, :update, :destroy]
  before_action :init_parent

  # GET /subdomain_configs
  # GET /subdomain_configs.json
  def index
    # @subdomain_configs = SubdomainConfig.all
  end

  # GET /subdomain_configs/1
  # GET /subdomain_configs/1.json
  def show
  end

  # GET /subdomain_configs/new
  def new
    # @subdomain_config = SubdomainConfig.new
  end

  # GET /subdomain_configs/1/edit
  def edit
  end

  # POST /subdomain_configs
  # POST /subdomain_configs.json
  def create
    # @subdomain_config = SubdomainConfig.new(subdomain_config_params)

    respond_to do |format|
      if @subdomain_config.save
        format.html { redirect_to [@domain_config, @subdomain_config], notice: 'Subdomain config was successfully created.' }
        format.json { render :show, status: :created, location: @subdomain_config }
      else
        format.html { render :new }
        format.json { render json: @subdomain_config.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subdomain_configs/1
  # PATCH/PUT /subdomain_configs/1.json
  def update
    respond_to do |format|
      if @subdomain_config.update(subdomain_config_params)
        format.html { redirect_to [@domain_config, @subdomain_config], notice: 'Subdomain config was successfully updated.' }
        format.json { render :show, status: :ok, location: @subdomain_config }
      else
        format.html { render :edit }
        format.json { render json: @subdomain_config.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subdomain_configs/1
  # DELETE /subdomain_configs/1.json
  def destroy
    @subdomain_config.destroy
    respond_to do |format|
      format.html { redirect_to domain_config_subdomain_configs_url(@domain_config), notice: 'Subdomain config was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subdomain_config
      # @subdomain_config = SubdomainConfig.find(params[:id])
    end

    def init_parent
      @parent_id = params[:domain_config_id]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subdomain_config_params
      params.require(:subdomain_config).permit(:domain_config_id, :subdomain, :result_lang_code)
    end
end
