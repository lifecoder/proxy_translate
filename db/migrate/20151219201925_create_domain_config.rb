class CreateDomainConfig < ActiveRecord::Migration
  def up
    create_table :domain_configs do |t|
      t.string :domain
      t.string :donor_url
      t.string :source_lang_code
      t.string :result_lang_code
      t.integer :development_mode, default: 0
    end

    domain = URI::parse(Setting.get('proxy_site_url')).host

    DomainConfig.create(
        domain: domain,
        donor_url: Setting.get('target_site_url'),
        source_lang_code: Setting.get('source_lang_code'),
        result_lang_code: Setting.get('result_lang_code'),
        development_mode: Setting.get('development_mode'),
    )
    Setting.where(key: 'active_domain').first_or_create!(value: domain)
  end

  def down
    drop_table :domain_configs
  end

  # def migrate(direction)
  #   super
  #
  #   if direction == 'up'
  #     DomainConfig.create(domain: default)
  #   end
  # end
end
