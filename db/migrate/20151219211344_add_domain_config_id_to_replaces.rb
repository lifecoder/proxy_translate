class AddDomainConfigIdToReplaces < ActiveRecord::Migration
  def change
    add_column :replaces, :domain_config_id, :integer
  end

  def migrate(direction)
    super

    if direction == :up
      Replace.update_all("domain_config_id = #{DomainConfig.first.id}")
    end
  end
end
