class AddPolymorphicToReplaces < ActiveRecord::Migration
  def change
    rename_column :replaces, :domain_config_id, :replaceable_id
    add_column :replaces, :replaceable_type, :string, default: 'DomainConfig'
  end
end
