class AddUserIdToDomainConfig < ActiveRecord::Migration
  def change
    add_column :domain_configs, :user_id, :integer
  end
end
