class CreateSubdomainConfigs < ActiveRecord::Migration
  def change
    create_table :subdomain_configs do |t|
      t.string :subdomain
      t.string :result_lang_code
      t.integer :domain_config_id

      t.timestamps null: false
    end
  end
end
