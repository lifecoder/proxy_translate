class AddBasicSettings < ActiveRecord::Migration
  def change
    Setting.where(key: 'target_site_url').first_or_create!(value: 'https://formkeep.com')
    Setting.where(key: 'proxy_site_url').first_or_create!(value: 'https://example-proxy-translator.com')
    Setting.where(key: 'source_lang_code').first_or_create!(value: 'en')
    Setting.where(key: 'result_lang_code').first_or_create!(value: 'ru')
  end
end
