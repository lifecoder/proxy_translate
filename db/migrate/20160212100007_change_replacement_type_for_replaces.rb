class ChangeReplacementTypeForReplaces < ActiveRecord::Migration
  def change
    change_column :replaces, :replacement, :text
  end
end
