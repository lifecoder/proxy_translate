class AddGlobalReplacementDefault < ActiveRecord::Migration
  def up
    add_column :global_replacements, :name, :string
    GlobalReplacement.create! name: 'global'
  end

  def down
    remove_column :global_replacements, :name
  end
end
