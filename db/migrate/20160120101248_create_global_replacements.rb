class CreateGlobalReplacements < ActiveRecord::Migration
  def change
    create_table :global_replacements do |t|

      t.timestamps null: false
    end
  end
end
