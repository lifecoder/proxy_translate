class AddDomainConfigIdToCacheSettings < ActiveRecord::Migration
  def change
    add_column :cache_settings, :domain_config_id, :integer
  end

  def migrate(direction)
    super

    if direction == :up
      CacheSetting.update_all("domain_config_id = #{DomainConfig.first.id}")
    end
  end
end
