class CreateCacheSettings < ActiveRecord::Migration
  def change
    create_table :cache_settings do |t|
      t.string :route_mask
      t.integer :expire_after, default: 300

      t.timestamps null: false
    end
  end
end
