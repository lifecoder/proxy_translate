class CreateReplaces < ActiveRecord::Migration
  def change
    create_table :replaces do |t|
      t.string :pattern
      t.string :replacement

      t.timestamps null: false
    end
  end
end
